package com.sample.test_web_app.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test_web_app")
public class TestWebAppController {

	@RequestMapping(path="/test", method = RequestMethod.GET)
	public String test() {
		return "Hello CircleCI :D";
	}
}
